<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\BlackJack;
use App\Service\prize\Money;
use App\Service\prize\Physical;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Environment;
use function basename;
use function get_class;
use function str_replace;

class ProfileController implements ContainerAwareInterface
{
    private $container;

    /**
     * @Route("/", name="index")
     */
    public function index(Environment $twig, AuthenticationUtils $authenticationUtils)
    {
        $authChecker = $this->container->get('security.authorization_checker');
        $response = new Response();
        $errors = $authenticationUtils->getLastAuthenticationError();

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            return $response->SetContent(
                $twig->render(
                    'admin/index.html.twig',
                    ['controller_name' => 'its GAME', 'error' => $errors]
                )
            );
        } elseif ($authChecker->isGranted('ROLE_USER')) {
            return $response->SetContent(
                $twig->render(
                    'profile/game.html.twig',
                    ['controller_name' => 'its GAME', 'error' => $errors]
                )
            );
        }

    }


    /**
     * @Route("/play", name="play")
     */
    public function letsPlay(Environment $twig, UserInterface $user, BlackJack $game)
    {
        $doctrine = $this->container->get("doctrine");

        $prize = $game->play($user, $doctrine);
        $prizeType = basename(
            str_replace(
                "\\",
                "/",
                get_class($prize)
            )
        );

        $responseData = [
            "success" => $prize->getExistAvailable(),
            'notice' => $prize->getNotice()
        ];

        if ($responseData["success"]) {

            if (get_class($prize) == Physical::class)
                $responseData["parameter"] = $prize->getPrizeId();
            if (get_class($prize) == Money::class)
                $responseData["parameter"] = $prize->getAmount();
        }

        return (new Response())
            ->SetContent(
                $twig->render(
                    "prize/$prizeType/index.html.twig",
                    $responseData
                )
            );

    }

    /**
     * Sets the container.
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
