<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Wallet;
use App\Service\ConverterMoneyToTokenService;
use App\Service\PaymentService;
use App\Service\prize\Token;
use App\Service\ShippingService;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class APIController implements ContainerAwareInterface
{
    private $container;

    /**
     * @Route("/payment", name="payment")
     * @param UserInterface $user
     * @param PaymentService $paypal
     * @return JsonResponse
     */
    public function pay(UserInterface $user, PaymentService $paypal)
    {
        $response = $paypal->sendMoneyToCard($user);

        if ($response == PaymentService::STATUS_SUCCESS) {
            $this
                ->container
                ->get('doctrine')
                ->getRepository(Wallet::class)
                ->refreshMoneyForUser($user);
        }
        return new JsonResponse(['result' => $response]);
    }

    /**
     * @Route("/convert/{amount}", name="convert")
     * @param $amount
     * @param UserInterface $user
     * @param ConverterMoneyToTokenService $moneyToTokenService
     * @return JsonResponse
     */
    public function convertMoneyToToken($amount, UserInterface $user, ConverterMoneyToTokenService $moneyToTokenService)
    {
        $doctrine = $this->container->get("doctrine");

        $moneyToTokenService
            ->convert(
                $doctrine,
                $user,
                (float)$amount,
                Token::COST
            );

        return new JsonResponse(['result' => 'success']);
    }

    /**
     * @Route("/shipping/{prize_id}", name="shipping")
     */
    public function shippingPrize($prize_id)
    {
        $doctrine = $this->container->get("doctrine");
        $result = (new ShippingService($doctrine))
            ->sendPrize((int)$prize_id);

        return new JsonResponse(['result' => "sent"]);
    }

    /**
     * @Route("/refuse/{prize_id}", name="refuse")
     */
    public function refusePhysicalPrize($prize_id)
    {
        $doctrine = $this->container->get("doctrine");
        $result = (new ShippingService($doctrine))
            ->returnBackPrize((int)$prize_id);

        return new JsonResponse(['result' => "ok"]);
    }

    /**
     * Sets the container.
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
