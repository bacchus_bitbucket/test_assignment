<?php
declare(strict_types=1);
namespace App\DataFixtures;

use App\Entity\PhysicalPrize;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PhysicalPrizeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $ball = (new PhysicalPrize())
            ->setName('ball')
            ->setStatus('available');

        $pen = (new PhysicalPrize())
            ->setName('pen')
            ->setStatus('available');

        $smartphone = (new PhysicalPrize())
            ->setName('smartphone')
            ->setStatus('available');

        $headphones = (new PhysicalPrize())
            ->setName('headphones')
            ->setStatus('available');

        $laptop = (new PhysicalPrize())
            ->setName('laptop')
            ->setStatus('available');

        $manager->persist($ball);
        $manager->persist($pen);
        $manager->persist($smartphone);
        $manager->persist($headphones);
        $manager->persist($laptop);

        $manager->flush();
    }
}
