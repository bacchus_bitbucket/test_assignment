<?php
declare(strict_types=1);
namespace App\DataFixtures;

use App\Entity\Wallet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class WalletFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $system_wallet = new Wallet();
        $system_wallet
            ->setUser($this->getReference(UserFixtures::REFERENCE_ADMIN))
            ->setMoneyAmount(1000)
            ->setTokenAmount(0);

        $manager->persist($system_wallet);

        $user_wallet = new Wallet();
        $user_wallet
            ->setUser($this->getReference(UserFixtures::REFERENCE_USER))
            ->setMoneyAmount(0)
            ->setTokenAmount(0);

        $manager->persist($user_wallet);
        $manager->flush();
    }

    public function getDependencies()
    {
        return ['App\DataFixtures\UserFixtures'];
    }
}
