<?php
declare(strict_types=1);
namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserFixtures extends Fixture implements ContainerAwareInterface
{

    const REFERENCE_ADMIN = 'admin';
    const REFERENCE_USER = 'user';
    private $container;

    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');

        $admin = new User();
        $user = new User();

        $admin->setUsername('admin')
            ->setPassword($encoder->encodePassword($admin, '0000'))
            ->setRole("ROLE_ADMIN");

        $user->setUsername('user')
            ->setPassword($encoder->encodePassword($user, '1111'))
            ->setRole("ROLE_USER");


        $this->addReference(self::REFERENCE_ADMIN, $admin);
        $this->addReference(self::REFERENCE_USER, $user);

        $manager->persist($admin);
        $manager->persist($user);

        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


}
