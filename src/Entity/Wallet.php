<?php
declare(strict_types=1);
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WalletRepository")
 */
class Wallet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="float")
     */
    private $money_amount;

    /**
     * @ORM\Column(type="float")
     */
    private $token_amount;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="wallet", cascade={"persist", "remove"})
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }



    public function getMoneyAmount(): ?float
    {
        return $this->money_amount;
    }

    public function setMoneyAmount(float $money_amount): self
    {
        $this->money_amount = $money_amount;

        return $this;
    }

    public function getTokenAmount(): ?float
    {
        return $this->token_amount;
    }

    public function setTokenAmount(float $token_amount): self
    {
        $this->token_amount = $token_amount;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        // set (or unset) the owning side of the relation if necessary
        $newWallet = $user === null ? null : $this;
        if ($newWallet !== $user->getWallet()) {
            $user->setWallet($newWallet);
        }

        return $this;
    }
}
