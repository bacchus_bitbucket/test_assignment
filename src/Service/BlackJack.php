<?php
declare(strict_types=1);

namespace App\Service;

use App\Interfaces\PrizeInterface;
use App\Service\prize\Factory;
use App\Service\prize\Money;
use App\Service\prize\Physical;
use App\Service\prize\Token;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

class BlackJack
{
    const PRIZES = [Token::class, Physical::class, Money::class];

    const TokenMax = 1000;

    public function play(UserInterface $user, ManagerRegistry $doctrine): PrizeInterface
    {
        $factory = new Factory();

        $prize_type = self::PRIZES[random_int(0, count(self::PRIZES) - 1)];
        $prize = $factory->createPrizeInstance($prize_type, $doctrine);

        return $prize->toIssuePrize($user, $doctrine);

    }

}