<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\User\UserInterface;
use function json_decode;

class PaymentService
{
    const STATUS_SUCCESS = "successful";
    const STATUS_FAIL = "fail";

    /**
     * @param UserInterface $user
     * @return string
     */
    public function sendMoneyToCard(UserInterface $user): string
    {
        $moneyAmount = $user->getWallet()->getMoneyAmount();
        $result = $this->sendRequest($moneyAmount, $user);
        $result = !empty($result) ? json_decode($result, true) : false;

        if ($result && $result['status'] == 'successful') {
            return self::STATUS_SUCCESS;
        } elseif ($result) {
            return $result['details'];
        }

        throw new Exception("couldn't make connection with bank. please ask support");
    }

    /**
     * @param float $amount
     * @param UserInterface $user
     * @return string
     */
    private function sendRequest(float $amount, UserInterface $user): string
    {
        /*
        $apiData = array(
            'name' => $user->getName(),
            ....
            'csv' =>$user->getPayInfo()->getScv(),
            'amount' => $this->amount
        );

        $data = json_encode($apiData);
        $url = "https://bank/api/transaction";
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            "x-api-key:23iurh2isdfsdfwefpowiejf"
        ));

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

         $result = curl_exec($ch);
        curl_close($ch);
        return $result;
        */

        return '{"status":"successful","details":""}';

    }
}