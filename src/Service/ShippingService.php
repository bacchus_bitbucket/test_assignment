<?php
declare(strict_types=1);

namespace App\Service;


use App\Entity\PhysicalPrize;
use App\Interfaces\ShippingInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ShippingService implements ShippingInterface
{
    const STATUS_AVAILABLE = 'available';
    const STATUS_PROCESSING = 'processing';
    const STATUS_SHIPPING = 'shipping';
    private $doctrine;
    private $prizeRep;

    /**
     * ShippingService constructor.
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->prizeRep = $doctrine->getRepository(PhysicalPrize::class);
    }

    /**
     * @param int $prizeId
     * @return bool
     */
    public function sendPrize(int $prizeId): bool
    {
        $object = $this
            ->prizeRep
            ->find($prizeId);

        $result = $this
            ->prizeRep
            ->changeStatus($object, self::STATUS_SHIPPING);

        return true;
    }

    /**
     * @param int $prizeId
     * @return bool
     */
    public function makeProcessing(int $prizeId): bool
    {
        $object = $this
            ->prizeRep
            ->find($prizeId);

        $result = $this
            ->prizeRep
            ->changeStatus($object, self::STATUS_PROCESSING);

        return true;
    }

    /**
     * @param int $prizeId
     * @return bool
     */
    public function returnBackPrize(int $prizeId): bool
    {
        $object = $this
            ->prizeRep
            ->find($prizeId);

        $result = $this
            ->prizeRep
            ->changeStatus($object, self::STATUS_AVAILABLE);
        return true;
    }

}