<?php
declare(strict_types=1);

namespace App\Service\prize;


use App\Entity\User;
use App\Entity\Wallet;
use App\Interfaces\ContentWalletInterface;
use App\Interfaces\ConvertibleInterface;
use App\Interfaces\LimitedAmountInterface;
use App\Interfaces\PrizeInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Money implements PrizeInterface, LimitedAmountInterface, ContentWalletInterface
{

    public $existAvailable;
    public $notice;
    public $amount;
    private $doctrine;
    private $walletRep;
    private $userRep;


    /**
     * Money constructor.
     * @param RegistryInterface $managerRegistry
     */
    public function __construct(RegistryInterface $managerRegistry)
    {
        $this->doctrine = $managerRegistry;
        $this->setExistAvailable(false);

        $this->walletRep = $this
            ->doctrine
            ->getRepository(Wallet::class);

        $this->userRep = $this
            ->doctrine
            ->getRepository(User::class);
    }

    public function toIssuePrize(UserInterface $user): PrizeInterface
    {
        $availableAmount = $this->getAvailableAmount();

        if ($availableAmount > 1) {
            $amountPrize = random_int(1, (int)$availableAmount - 1);

            $this->updateDB($availableAmount - $amountPrize, 'admin');
            $this->updateWalletContent($amountPrize, $user);
            $this->setExistAvailable(true);
            $this->setNotice("you win $amountPrize \$");
            $this->setAmount($amountPrize);
            return $this;
        }
        $this->setNotice("sorry, money are over");
        return $this;
    }

    public function getAvailableAmount(): float
    {
        $user = $this
            ->userRep
            ->findOneByUserName('admin');

        $system_wallet = $this
            ->walletRep
            ->getWalletByUserId(
                $user->getId()
            );

        return $system_wallet ? $system_wallet->getMoneyAmount() : 0;
    }

    public function updateDB($amount, $user_name): bool
    {
        $user = $this
            ->userRep
            ->findOneByUserName($user_name);

        $this->walletRep
            ->updateMoneyByUserId(
                $amount,
                $user->getId()
            );
        return true;
    }

    public function updateWalletContent(float $increment, UserInterface $user): bool
    {
        $current_money = $this
            ->walletRep
            ->getMoneyByUserId(
                $user->getId()
            );

        $this->updateDB(
            $current_money + $increment,
            $user->getUsername()
        );

        return true;
    }

    /**
     * @return mixed
     */
    public function getExistAvailable(): bool
    {
        return $this->existAvailable;
    }

    /**
     * @param mixed $existAvailable
     * @return Money
     */
    public function setExistAvailable($existAvailable): self
    {
        $this->existAvailable = $existAvailable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNotice(): string
    {
        return $this->notice;
    }

    /**
     * @param mixed $notice
     * @return Money
     */
    public function setNotice($notice): self
    {
        $this->notice = $notice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return Money
     */
    public function setAmount($amount): self
    {
        $this->amount = $amount;
        return $this;
    }

}