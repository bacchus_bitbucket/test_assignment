<?php
declare(strict_types=1);
namespace App\Service\prize;


use App\Interfaces\PrizeInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class Factory
{
    public function createPrizeInstance(string $className,RegistryInterface $doctrine): PrizeInterface
    {
        if(class_exists($className)){
            return new $className($doctrine);
        }else{
            throw new Exception("class $className doesnt exist");
        }
    }

}