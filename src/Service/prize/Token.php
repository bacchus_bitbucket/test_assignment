<?php
declare(strict_types=1);

namespace App\Service\prize;

use App\Entity\Wallet;
use App\Interfaces\ContentWalletInterface;
use App\Interfaces\PrizeInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Token implements PrizeInterface, ContentWalletInterface
{

    CONST LIMIT = 1000;
    CONST COST = 0.2;
    public $existAvailable;
    public $notice;
    private $doctrine;

    /**
     * TokenPrizeService constructor.
     * @param RegistryInterface $managerRegistry
     */
    public function __construct(RegistryInterface $managerRegistry)
    {
        $this->doctrine = $managerRegistry;
        $this->setExistAvailable(true);
    }

    /**
     * @param UserInterface $user
     * @return PrizeInterface
     * @throws \Exception
     */
    public function toIssuePrize(UserInterface $user): PrizeInterface
    {
        $amount = random_int(1, Token::LIMIT);
        $this->updateWalletContent($amount, $user);
        $this->setNotice("you win $amount tokens!");
        return $this;
    }

    /**
     * @param float $increment
     * @param UserInterface $user
     * @return bool
     */
    public function updateWalletContent(float $increment, UserInterface $user): bool
    {
        $walletRep = $this
            ->doctrine
            ->getRepository(Wallet::class);

        $current_tokens = $walletRep
            ->getTokenByUserId(
                $user->getId()
            );

        $walletRep->updateTokenByUserId(
                $current_tokens + $increment,
                $user->getId()
            );
        return true;
    }

    /**
     * @return float
     */
    public function getCoefficient(): float
    {
        return Token::COST;
    }

    /**
     * @return mixed
     */
    public function getExistAvailable(): bool
    {
        return $this->existAvailable;
    }

    /**
     * @param mixed $existAvailable
     * @return Token
     */
    public function setExistAvailable($existAvailable): self
    {
        $this->existAvailable = $existAvailable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNotice(): string
    {
        return $this->notice;
    }

    /**
     * @param mixed $notice
     * @return Token
     */
    public function setNotice($notice): self
    {
        $this->notice = $notice;
        return $this;
    }


}
