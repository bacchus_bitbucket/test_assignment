<?php
declare(strict_types=1);

namespace App\Service\prize;


use App\Entity\PhysicalPrize;
use App\Interfaces\LimitedCollectionInterface;
use App\Interfaces\PrizeInterface;
use App\Interfaces\ReturnableInterface;
use App\Service\ShippingService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class Physical implements PrizeInterface, LimitedCollectionInterface
{

    public $existAvailable;
    public $notice;
    public $prizeId;
    private $doctrine;
    private $rep;
    private $shipping;


    /**
     * Physical constructor.
     * @param RegistryInterface $managerRegistry
     */
    public function __construct(RegistryInterface $managerRegistry)
    {
        $this->doctrine = $managerRegistry;
        $this->rep = $managerRegistry
            ->getRepository(PhysicalPrize::class);
        $this->setExistAvailable(false);
        $this->shipping = new ShippingService($this->doctrine);

    }

    /**
     * @param UserInterface $user
     * @return PrizeInterface
     * @throws \Exception
     */
    public function toIssuePrize(UserInterface $user): PrizeInterface
    {
        $available_things = $this->getAvailableAmount();
        $this->existAvailable = count($available_things) > 0 ? true : false;

        if ($this->existAvailable) {
            $prize_index = random_int(
                0,
                count($available_things) - 1
            );

            $prize = $available_things[$prize_index];

            $this->setExistAvailable(true);
            $this->setNotice("you win " . $prize->getName());
            $this->setPrizeId($prize->getId());

            $this->shipping->makeProcessing($this->prizeId);

            return $this;
        }

        $this->setNotice("sorry, prizes are over");
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAvailableAmount(): ArrayCollection
    {
        return $this->rep->getByStatus(ShippingService::STATUS_AVAILABLE);
    }

    /**
     * @return bool
     */
    public function getExistAvailable(): bool
    {
        return $this->existAvailable;
    }

    /**
     * @param mixed $existAvailable
     * @return Physical
     */
    public function setExistAvailable($existAvailable): self
    {
        $this->existAvailable = $existAvailable;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNotice(): string
    {
        return $this->notice;
    }

    /**
     * @param mixed $notice
     * @return Physical
     */
    public function setNotice($notice): self
    {
        $this->notice = $notice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrizeId(): int
    {
        return $this->prizeId;
    }

    /**
     * @param mixed $prizeId
     * @return Physical
     */
    public function setPrizeId($prizeId): self
    {
        $this->prizeId = $prizeId;
        return $this;
    }
}