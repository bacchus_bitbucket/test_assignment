<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Wallet;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

class ConverterMoneyToTokenService
{
    /**
     * @param ManagerRegistry $doctrine
     * @param UserInterface $user
     * @param float $moneyAmount
     * @param float $cost
     * @return bool
     */
    public function convert(ManagerRegistry $doctrine, UserInterface $user, float $moneyAmount, float $cost): bool
    {
        $walletRepository = $doctrine->getRepository(Wallet::class);
        $currentMoneyAmount = $user
            ->getWallet()
            ->getMoneyAmount();

        $currentTokenAmount = $user
            ->getWallet()
            ->getTokenAmount();

        $tokens = $moneyAmount * $cost;
        $newMoneyAmount = $currentMoneyAmount - $moneyAmount;
        $newTokenAmount = $currentTokenAmount + $tokens;

        $walletRepository
            ->updateTokenByUserId(
                $newTokenAmount,
                $user->getId()
            );

        $walletRepository
            ->updateMoneyByUserId(
                $newMoneyAmount,
                $user->getId()
            );

        return true;
    }

}