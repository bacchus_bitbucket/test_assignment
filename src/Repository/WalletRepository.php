<?php

namespace App\Repository;

use App\Entity\Wallet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Wallet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wallet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wallet[]    findAll()
 * @method Wallet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WalletRepository extends ServiceEntityRepository
{
    private $doctrine;

    public function __construct(RegistryInterface $registry)
    {
        $this->doctrine = $registry;
        parent::__construct($registry, Wallet::class);
    }

    /**
     * @param $value
     * @param $user_id
     * @return Wallet|null
     */
    public function updateMoneyByUserId($value, $user_id): ?Wallet
    {
        $wallet = $this->getWalletByUserId($user_id);

        if ($wallet) {
            $wallet->setMoneyAmount($value);

            $doctrineManager = $this->doctrine->getManager();
            $doctrineManager->persist($wallet);
            $doctrineManager->flush();

            return $wallet;
        }
        throw new Exception("user with id:$user_id not exist");
    }

    /**
     * @param $value
     * @param $user_id
     * @return Wallet|null
     */
    public function updateTokenByUserId($value, $user_id): ?Wallet
    {

        $wallet = $this->getWalletByUserId($user_id);

        if ($wallet) {
            $wallet->setTokenAmount($value);

            $doctrineManager = $this->doctrine->getManager();
            $doctrineManager->persist($wallet);
            $doctrineManager->flush();

            return $wallet;
        }
        throw new Exception("user with id:$user_id not exist");
    }

    /**
     * @param $id
     * @return Wallet|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getWalletByUserId($id): ?Wallet
    {
        return $this->createQueryBuilder('w')
            // p.category refers to the "category" property on product
            ->innerJoin('w.user', 'u')
            // selects all the category data to avoid the query
            ->addSelect('u')
            ->andWhere('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();

    }

    /**
     * @param $user_id
     * @return float
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTokenByUserId($user_id): float
    {
        $wallet = $this->getWalletByUserId($user_id);
        if ($wallet) {
            return $wallet->getTokenAmount();
        }
        throw new Exception("user with id:$user_id not exist");
    }

    /**
     * @param $user_id
     * @return float
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMoneyByUserId($user_id):float
    {
        $wallet = $this->getWalletByUserId($user_id);
        if ($wallet) {
            return $wallet->getMoneyAmount();
        }
        throw new Exception("user with id:$user_id not exist");
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    public function refreshMoneyForUser(UserInterface $user):bool
    {
        $this->updateMoneyByUserId(0, $user->getId());
        return true;
    }


}
