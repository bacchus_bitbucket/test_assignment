<?php
declare(strict_types=1);
namespace App\Repository;

use App\Entity\PayDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PayDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method PayDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method PayDetails[]    findAll()
 * @method PayDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PayDetailsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PayDetails::class);
    }


}
