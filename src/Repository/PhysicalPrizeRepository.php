<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\PhysicalPrize;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PhysicalPrize|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhysicalPrize|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhysicalPrize[]    findAll()
 * @method PhysicalPrize[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhysicalPrizeRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PhysicalPrize::class);
    }

    /**
     * @param string $status
     * @return ArrayCollection
     */
    public function getByStatus($status = 'free'): ArrayCollection
    {
        $data = $this->createQueryBuilder('p')
            ->andWhere('p.status = :val')
            ->setParameter('val', $status)
            ->getQuery()
            ->getResult();
        return new ArrayCollection($data);
    }

    /**
     * @param $prizeId
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function returnPrizeBack($prizeId): bool
    {
        $prize = $this->find($prizeId);

        /*
         * some logic
         */
        $this->changeStatus(
            $prize,
            PhysicalPrize::STATUS_AVAILABLE
        );

        return true;
    }

    /**
     * @param PhysicalPrize $thing
     * @param string $status
     * @return PhysicalPrize
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function changeStatus(PhysicalPrize $thing, string $status): PhysicalPrize
    {
        $thing->setStatus($status);

        $en = $this->getEntityManager();
        $en->persist($thing);
        $en->flush();
        return $thing;
    }


}
