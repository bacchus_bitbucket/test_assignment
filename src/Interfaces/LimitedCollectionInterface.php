<?php
declare(strict_types=1);

namespace App\Interfaces;


use Doctrine\Common\Collections\ArrayCollection;

interface LimitedCollectionInterface
{
    /**
     * @return ArrayCollection
     */
    public function getAvailableAmount(): ArrayCollection;

}