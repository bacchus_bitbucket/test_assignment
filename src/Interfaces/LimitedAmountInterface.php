<?php
declare(strict_types=1);

namespace App\Interfaces;


interface LimitedAmountInterface
{
    /**
     * @return float
     */
    public function getAvailableAmount(): float;

}