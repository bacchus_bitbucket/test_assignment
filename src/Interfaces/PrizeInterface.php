<?php

namespace App\Interfaces;


use Symfony\Component\Security\Core\User\UserInterface;

Interface PrizeInterface
{
    public function toIssuePrize(UserInterface $user): PrizeInterface;

    public function getNotice(): string;

    public function getExistAvailable(): bool;

}