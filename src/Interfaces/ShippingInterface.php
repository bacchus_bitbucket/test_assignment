<?php
declare(strict_types=1);

namespace App\Interfaces;


interface ShippingInterface
{
    public function sendPrize(int $prizeId);

    public function returnBackPrize(int $prizeId);
}