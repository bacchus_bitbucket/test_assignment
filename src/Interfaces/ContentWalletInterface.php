<?php
declare(strict_types=1);
namespace App\Interfaces;


use Symfony\Component\Security\Core\User\UserInterface;

interface ContentWalletInterface
{
    public function updateWalletContent(float $amount, UserInterface $user): bool;
}